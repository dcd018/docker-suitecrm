#### Usage

If containers fail to build due to port conflicts, you may have to stop your local MySQL server listening on port 3306 or modify listen ports within `docker-compose.yml`.

```sh
git clone git@gitlab.com:dcd018/docker-suitecrm.git
cd docker-suitecrm
./init_hooks
git checkout master
docker-compose up -d
```

Once containers are built, periodically run `docker-compose ps`, check the status of the `composer` container and wait until composer is done installing dependencies (`State: Exit 0`).

In a browser, navigate to http://localhost:8080/install.php

#### Installation

##### Database Configuration

```
Database Name: suitecrm
Host Name: db #container alias
User: docker
Password: docker
```

##### Site Configuration

```
URL of SuiteCRM Instance: http://localhost:8080
```

##### File Permissions

Once installation finishes, you should get an output buffer warning by clicking next. Navigate to http://localhost:8080. Upon first load, the site's `cache` directory is generated. You should notice the login view is missing styles because requests including the `cache` directory are responding with  403 Forbidden errors. Open a terminal and change permissions of that directory.

```
cd docker-suitecrm/app/src
chmod -R 775 cache
```

Confirm permissions reflect via shared volume by navigating to http://localhost:8080 again.
